# WiLED: Housing CAD model for Hektar lamp

FreeCAD 3D model to house version 3.0 of the WiLED PCB and attach it to an IKEA Hektar floor-standing lamp. 

![Side view](side.png)

![PCB view](rear.png)


# Licence

All hardware models and design files are released under the CERN Open Hardware Licence v1.2. Please see [LICENCE](LICENCE) for details.
